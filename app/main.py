# coding: utf8
from flask import Flask, session, render_template, request, abort, redirect, url_for, flash, jsonify, Response
from functools import wraps
import pymongo
from pymongo import MongoClient
import uuid
from flask_bcrypt import generate_password_hash, check_password_hash
import json
from bson.json_util import dumps, loads
from dicttoxml import dicttoxml
from lxml import etree
from werkzeug import secure_filename
import time

app = Flask(__name__)

SECRET_KEY = "CHANGE_ME"
parser = etree.XMLParser(no_network=False)
app.config.from_object(__name__)

connection = MongoClient("boards-db", 27017, connect=False)
db = connection.service

def get_user():
	login = 'user' in session	
	if login:
		login=session['user']
		return (True, login)
	return (False, None)
	
def login_required(f):
	@wraps(f)
	def decorated_function(*args, **kwargs):
		if 'user' not in session:
			flash('Login please!')
			return redirect(url_for('login'))
		return f(*args, **kwargs)
	return decorated_function

def session_login(username):
	session['user'] = username

@app.route('/logout')
@login_required
def logout():
	del session['user']
	return redirect('/')

@app.route('/support_panel', methods=['GET', 'POST'])
def support_panel():
	if request.headers.get('Remote-Addr') == '127.0.0.1' or get_user()[1] == 'support':
		if request.method == 'GET':
			ticket_id = request.args.get('id')
			ticket = ''
			if ticket_id:
				ticket = db.support.find_one({'id':int(ticket_id)})
			tickets = db.support.find({}).sort('time',-1)
			return	render_template('support_panel.html',tickets=tickets,ticket=ticket)
		elif request.method == 'POST':
			ticket_id = request.form['ticket_id']
			text = request.form['text']
			if text:
				db.support.update({ 'id': int(ticket_id)},{ '$push': { 'comments': { "support":text } } })
				return redirect('/support_panel?id='+ticket_id)
			return redirect(url_for('support_panel'))
		else:
			abort(405)
	return redirect(url_for("login"))

@app.route('/all_boards', methods=['GET', 'POST'])
def all_boards():
	if request.headers.get('Remote-Addr') == '127.0.0.1' or get_user()[1] == 'support':
		if request.method == 'GET':
			return render_template('all_boards.html')
		else:
			abort(405)
	else:
		return redirect(url_for('all_boards'))

@app.route('/get_all_boards', methods=['GET'])
def get_all_boards():
	if request.headers.get('Remote-Addr') == '127.0.0.1' or get_user()[1] == 'support':
		boards = db.boards.find({})
		return dumps(boards)
	else:
		abort(405)

@app.route('/support', methods=['GET', 'POST'])
@login_required
def support():
	if request.method == 'GET':
		ticket_id = request.args.get('id')
		tickets = db.support.find({'author':get_user()[1]})
		ticket = ''
		if ticket_id:
			ticket = db.support.find_one({'id':int(ticket_id)})
		return render_template('support.html', tickets=tickets, ticket=ticket)
	elif request.method == 'POST':
		ticket_id = request.form['ticket_id']
		text = request.form['text']
		if text:
			db.support.update({ 'id': int(ticket_id), 'author': get_user()[1] },{ '$push': { 'comments': { get_user()[1]:text } } })
			return redirect('/support?id='+ticket_id)
		return redirect(url_for('support'))
	else:
		abort(405)

@app.route('/get_boards/<max_id>', methods=['GET'])
def get_boards(max_id):
	boards_filter = '{"status":"public","id":{"$gt":'+max_id+'}}'
	boards = db.boards.find(loads(boards_filter),{"_id":0}).sort('id',1).limit(1)
	return dumps(boards)

@app.route('/add_comment', methods=['POST'])
def add_comment():
	if request.method == 'POST':
		comment = request.form['comment']
		id_comment = int(request.form['id'])
		db.boards.update({ 'id':id_comment }, { '$push': { 'comments': comment} })
		return redirect('/')
	else:
		abort(405)

@app.route('/like/<int:id_board>', methods=['GET'])
def add_like(id_board):
	if request.method == 'GET':
		if get_user()[0]:
			if get_user()[1] in db.boards.find_one({'id':id_board},{'likes':1,'_id':0})['likes']:
				db.boards.update( {'id':id_board}, { '$pull': { 'likes': get_user()[1]} } )
			else:
				db.boards.update({ 'id':id_board }, { '$push': { 'likes': get_user()[1]}})
			count_likes = db.boards.aggregate([
			{"$match": 
				{"id":int(id_board)}
			},
			{"$project":
				{"_id":0,"likes":{"$size": '$likes'}}
			}])
			return jsonify(mess=[e for e in count_likes][0]['likes'])
		else:
			return jsonify(mess="error")
	else:
		abort(405)

@app.route('/create_board', methods=['GET', 'POST'])
@login_required
def create_board():
	if request.method == 'GET':
		return render_template('create_board.html')
	elif request.method == 'POST':
		title = request.form['title']
		text = request.form['textarea']
		status = request.form['PublicOrPrivat']
		maximum = db.boards.find({},{'id':1}).sort('id',-1).limit(1)
		if maximum.count():
			db.boards.insert({'author':get_user()[1],'title':title,'text':text,'status':status,'likes':[],'comments':[],'id':maximum[0]['id']+1})
		else:
			db.boards.insert({'author':get_user()[1],'title':title,'text':text,'status':status,'likes':[],'comments':[],'id':1})
		return render_template('create_board.html')
	else:
		abort(405)

@app.route('/create_ticket', methods=['GET', 'POST'])
@login_required
def create_ticket():
	if request.method == 'GET':
		board_id = request.args.get('id')
		board = ''
		if board_id:
			board = db.boards.find_one({'author':get_user()[1], 'id':int(board_id)})
		return render_template('create_ticket.html', board=board)
	elif request.method == 'POST':
		title = request.form['title']
		text = request.form['textarea']
		maximum = db.support.find({},{'id':1}).sort('id',-1).limit(1)
		if maximum.count():
			db.support.insert({'author':get_user()[1],'title':title,'text':text,'time':time.time(),'comments':[],'id':maximum[0]['id']+1})
		else:
			db.support.insert({'author':get_user()[1],'title':title,'text':text,'time':time.time(),'comments':[],'id':1})
		return redirect(url_for('support'))
	else:
		abort(405)

@app.route('/delete_board/<int:id_board>', methods=['GET'])
@login_required
def delete_board(id_board):
	if request.method == 'GET':
		db.boards.delete_one({'author':get_user()[1],'id':id_board})
		return redirect(url_for('my_boards'))
	else:
		abort(405)

@app.route('/support_delete_board/<int:id_board>', methods=['GET'])
@login_required
def support_delete_board(id_board):
	if request.headers.get('Remote-Addr') == '127.0.0.1' or get_user()[1] == 'support':
		if request.method == 'GET':
			db.boards.delete_one({'id':id_board})
			return redirect(url_for('all_boards'))
		else:
			abort(405)
	return redirect(url_for('all_boards'))

@app.route('/my_boards', methods=["GET","POST"])
@login_required
def my_boards():
	if request.method == 'GET':
		boards = db.boards.find({'author':get_user()[1]},{"_id":0,"title":1,'text':1,'likes':1,'comments':1,'status':1,'id':1})
		return render_template('my_boards.html',boards=boards)

@app.route('/save_board/<int:id_board>', methods=["GET"])
@login_required
def save_board(id_board):
	if request.method == 'GET':
		board = db.boards.find_one({'author':get_user()[1],'id':id_board},{"_id":0,"title":1,'text':1,'status':1})
		if not board:
			flash('Wrong id! Hacker detectid!')
			return redirect(url_for('my_boards'))
		else:
			print board
			print dicttoxml(board)
			return Response(dicttoxml(board),mimetype='application/xml', headers={"Content-Disposition":"attachment;filename=board.xml"})
	else:
		abort(405)

@app.route('/support_save_board/<int:id_board>', methods=["GET"])
@login_required
def support_save_board(id_board):
	if request.headers.get('Remote-Addr') == '127.0.0.1' or get_user()[1] == 'support':
		if request.method == 'GET':
			board = db.boards.find_one({'id':id_board},{"_id":0,"title":1,'text':1,'status':1})
			if not board:
				return redirect(url_for('all_boards'))
			else:
				print board
				print dicttoxml(board)
				return Response(dicttoxml(board),mimetype='application/xml', headers={"Content-Disposition":"attachment;filename=board.xml"})
		else:
			abort(405)
	return redirect(url_for('all_boards'))


@app.route('/upload_board', methods=["POST"])
@login_required
def upload_board():
	if request.method == 'POST':
		xml = request.files['file'].stream.read()
		try:
			root = etree.fromstring(xml,parser)
			board = {}
			for appt in root.getchildren():
				board[appt.tag] = appt.text
			maximum = db.boards.find({},{'id':1}).sort('id',-1).limit(1)
			if maximum.count():
				db.boards.insert({'author':get_user()[1],'title':board['title'],'text':board['text'],'status':board['status'],'likes':[],'comments':[],'id':maximum[0]['id']+1})
			else:
				db.boards.insert({'author':get_user()[1],'title':board['title'],'text':board['text'],'status':board['status'],'likes':[],'comments':[],'id':1})
			return redirect(url_for("my_boards"))
		except:
			flash("Eror file type!")
			return redirect(url_for("my_boards"))
	else:
		abort(405)

@app.route('/register', methods=['GET', 'POST'])
def register():
	if request.method == 'GET':
		return render_template('register.html')
	elif request.method == 'POST':
		username = request.form['txtUsername']
		password = request.form['txtPassword']
		user=db.users.find_one({'user':username})
		if not user:
			db.users.insert({'user':username,'password':generate_password_hash(password),'reset_password':0})
			session_login(username)
			return redirect('/')
		else: 
			flash('The username {0} is already in use.  Please try a new username.'.format(username))
			return redirect(url_for('register'))
	else:
		abort(405)

@app.route('/about', methods=['GET'])
def about():
	return render_template('about.html')

@app.route('/login', methods=['GET', 'POST'])
def login():
	if request.method == 'GET':
		return render_template('login.html')
	elif request.method == 'POST':
		username = request.form['txtUsername']
		passw = request.form['txtPassword']
		user = db.users.find_one({'user':username})
		if not user:
			flash('Invalid login or password')
			return render_template('login.html')
		else:
			if user['reset_password']:
				return redirect('/reset_passwd?user='+username)
			if check_password_hash(user['password'],passw):
				session_login(username)
				if username == 'support':
					return redirect(url_for('support_panel'))
				return redirect(url_for('index'))
			else: 
				flash('Invalid login or password')
				return render_template('login.html')
	else:
		return abort(405)

@app.route('/reset_passwd', methods=["GET","POST"])
def reset_passwd():
	if request.method == 'GET':
		return render_template('reset_passwd.html',user=request.args.get('user'))
	elif request.method == 'POST':
		username = request.form['txtUsername']
		passw = request.form['txtPassword']
		new_passwd = request.form['txtNewPassword']
		user = db.users.find_one({'user':username})
		if not user:
			flash('Invalid login')
			return render_template('login.html')
		else:
			if check_password_hash(user['password'],passw):
				db.users.update({ 'user':username }, { "$set": { 'password':generate_password_hash(new_passwd),'reset_password':0 } })
				session_login(username)
				return redirect(url_for('index'))
			else:
				render_template('login.html')
	else:
		return abort(405)

@app.route('/')
def index():
	if request.method == 'GET':
		public_boards = db.boards.aggregate([
			{"$match": 
				{"status":"public"}
			},
			{"$project":
				{"_id":0,"text":1,"title":1,"comments":1,"id":1,"likes":{"$size": '$likes'}}
			},
			{"$sort":
				{"likes": -1}
			}
		])
		all_boards = [ e for e in public_boards]
		if all_boards:
			return render_template('index.html',boards=all_boards)
		else:
			return redirect(url_for('about'))

if __name__ == "__main__":
	app.run(host="0.0.0.0", port=80, debug=True)
