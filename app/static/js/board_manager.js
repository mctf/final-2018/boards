setInterval(update_board, 3000);

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

//async function demo() {
//  console.log('Taking a break...');
//  await sleep(2000);
//  console.log('Two seconds later');
//}

function escapeHTML (unsafe_str) {
    return unsafe_str
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;')
}

function generate_new_board(json){
	var board =  '<div class="container"><div  item_id="'+json['id']+'" class="alert alert-success" role="alert"> 		<h2 class="alert-heading">'+json['title']+'</h2> 		<p>'+json['text']+'</p>  		<div class="span4 collapse-group">  			<div class="collapse" id="viewdetails'+json['id'].toString()+'"> 		<hr>		<form class="form-inline" action="/add_comment" method="POST"> 					<div class="form-group mx-sm-3 mb-2"> 						<label for="comment" class="sr-only">Comment:</label> 			<input type="hidden" value="'+json['id'].toString()+'" name="id">			<input type="text" class="form-control" name="comment" id="comment" placeholder="add comment..."> 					</div> 					<button type="submit" class="btn btn-info mb-2">Add</button> 				</form> 			</div> 			<p> 				<a class="btn" data-toggle="collapse" data-target="#viewdetails'+json['id'].toString()+'">View comments ('+json['comments'].length.toString()+') &raquo;</a> 				<button class="btn btn-default btn-lg btn-link" id="'+json['id'].toString()+'"  onclick="like(id);"> 					<span class="glyphicon glyphicon-thumbs-up"></span> 				</button> 				<span class="badge badge-notify" id="like'+json['id'].toString()+'">'+json['likes'].length.toString()+'</span> 			</a> 			</p> 		</div>  	</div>   </div>' 

	return board
}

function like(id){
  $.ajax({url: "/like/"+id, success: function(result){
        if (result['mess'] === 'error'){
          mess = '<div  id="error" class="alert alert-danger" role="alert">Only registered users can like!</div>'
          $('.boards').prepend(mess)
          setTimeout(function(){$("#error").fadeOut();}, 1000);
          }
        else{
          $('#like'+id).text(result['mess'].toString());
        }
        }
    });
}

function update_board() {
  var arr_from_json = null
  var numbers = $(".boards").children().children().map(function(){
      return parseFloat(this.getAttribute('item_id'));
  }).toArray();
  if (numbers.length)
    var max = Math.max.apply(Math, numbers);
  else
    var max = 0


	$.ajax({url: "/get_boards/"+max.toString(), success: function(result){
        result = escapeHTML(result)
        if (result.length>2){
          arr_from_json = JSON.parse( result )[0];
          board = generate_new_board(arr_from_json)
          $('.boards').prepend(board)
        }
    }});



}
