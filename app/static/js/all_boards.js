update_board()
setInterval(update_board, 3000);

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

//async function demo() {
//  console.log('Taking a break...');
//  await sleep(2000);
//  console.log('Two seconds later');
//}

function escapeHTML (unsafe_str) {
    return unsafe_str
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;')
}

function generate_new_board(json){
	var board =  '<tr><td>'+json["id"]+'</td><td>'+json["author"]+'</td><td>'+json["status"]+'</td><td>'+json["title"]+'</td><td>'+json["text"]+'</td><td>'
          for (var i = 0;i < json['comments'].length; i++ ){
            board += '<p style="font-size: 16px;">'+json['comments'][i]+'</p>'
          }
          board += '</td><td>'
          for (var i = 0;i < json['likes'].length; i++ ){
            board += '<p style="font-size: 16px;">'+json['likes'][i]+'</p>'
          }
          board += '</td><td><a href="/save_board/'+json["id"]+'"><span class="glyphicon glyphicon-floppy-save"></span></a></span></a> <a href="/delete_board/'+json["id"]+'"> <span class="glyphicon glyphicon-trash"></span></a></td></tr>'
	return board
}

function update_board() {

	$.ajax({url: "/get_all_boards", success: function(result){
        $('.boards').empty()
        result = escapeHTML(result)
        arr_from_json = JSON.parse( result )
        for (var board = 0; board < arr_from_json.length; board++){
          new_board = generate_new_board(arr_from_json[board])
          $('.boards').prepend(new_board)
        //if (result.length>2){
        //  arr_from_json = JSON.parse( result )[0];
        //  board = generate_new_board(arr_from_json)
        //  $('.boards').prepend(board)
        }
    }});



}
